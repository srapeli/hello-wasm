# hello wasm
This repository aims to be a simple tutorial / example for getting started with
WebAssembly (wasm), especially when using C as the language.

## Useful resources
### Emscripten
Emscripten is used to compile the C source files to wasm.

[Emscripten installation instructions](https://emscripten.org/docs/getting_started/downloads.html)

### MDN
[Compiling a New C/C++ Module to WebAssembly](https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm)

## Compiling
```bash
# compile c to wasm
emcc hello.c -o hello.html
# serve the files (just opening in browser won't work)
# other web servers can be used as well
npx serve -p 3000 .
firefox http://localhost:3000/
```
